function ($locationProvider, $httpProvider, $controllerProvider) {
    const appUrlBase = '/app.html';
    
    const PULL_INTERVAL = 5000;
    
    $locationProvider.html5Mode({enabled:true, requireBase:true});
    
    $controllerProvider.register('PantryController', function($scope, $http, $location, $window, NoonWebService, $interval) {
        
        $scope.passcode = 1234;
        
        const sfx = {
            '-1': new Audio('sfx/blip_rev.mp3'),
            '1': new Audio('sfx/blip.mp3'),
            '2':  new Audio('sfx/blip2x.mp3')
        };
        // const sfxUp = new Audio('sfx/robotBlip.mp3');
        
        const playing = a=>(a.duration > 0 && !a.paused);
        const play = a=>{
            if(!a) return;
            if(playing(a)) {
                a.currentTime = 0;
            }
            else {
                a.play();
            }
        }
        
        var serviceDay, updating = false;
        
        const pullData = function(pc) {
            if(updating) return;
            var params = pc ? {passcode:pc} : {service_day:serviceDay._id};
            
            // console.log('init', params);
            return NoonWebService.call('pantry/init', params).then(r=>{
                
                var prevStations = serviceDay && serviceDay.stations || [];
                
                serviceDay = $scope.serviceDay = r;
                $scope.currOrg = r.org._disp;
                // console.log('response', r);
                
                if(!$scope.stations) {
                    $scope.stations = serviceDay.stations;
                }
                let idx = 0;
                _.forEach(r.stations, s=>{
                    s.idx = idx++;
                    // console.log('-->', s);
                    s.editing = true;
                    _.forEach(s.station.served_items, i=>{
                        if(i.quantity_available !== undefined) {
                            s.editing = false;
                        }
                        else {
                            i.quantity_available = '';
                        }
                    });
                });
                
                // var stationPos = prevStations.indexOf($scope.currScreen);
                // if(stationPos > -1) {
                //     $scope.currScreen = serviceDay.stations[stationPos];
                //     var s = prevStations[stationPos];
                //     $scope.currScreen.station.served_items  = s.station.served_items;
                //     $scope.currScreen.editing = s.editing;
                // }
            },
            err=>{
                console.log('error', err);
            })
        };
        
        
        const updateData = function(promised, served, inv_update) {
            updating = true;
            
            var params = {service_day:serviceDay._id, promised, served, inv_update};
            console.log('update', params);
            return NoonWebService.call('pantry/update', params).then(r=>{
                updating = false;
            },
            err=>{
                console.log('error', err);
            })
        }
        
        $scope.submitPasscode = function(pc) {
            pullData(pc).then(()=>{
                $scope.currScreen = 'home';
                $interval(pullData.bind(null,null), PULL_INTERVAL);
            });
        };
        
        $scope.switchScreen = function(scr) {
            $scope.currScreen = scr;
            if(typeof scr === 'number') {
                console.log('Station screen');
            }
        }
        
        $scope.servedChg = function(diff) {
            serviceDay.served += diff;
            play(sfx[diff]);
            updateData(null, serviceDay.served, null);
        };
        
        $scope.promChg = function(diff) {
            serviceDay.promised += diff;
            play(sfx[diff]);
            updateData(serviceDay.promised, null, null);
        };
        
        $scope.toggleEditInventory = function(s) {
            s.editing = !s.editing;
        
        };
        
    });
}