function(queryParams,db, _) {
    const sd = queryParams.service_day;
    
    const invUpdate = queryParams.inv_update;
    const served = queryParams.served;
    const promised = queryParams.promised;
    
    
    if(!sd || !(invUpdate || served || promised)){
        return {error:'missing required params', invUpdate, served, promised};
    }
    
    return db.ServiceDay.findOne({_id:sd}).then(sd=>{
        if(!sd) {
            return {error:'invalid service_day (no active service day found'};
        }
        if(served) {
            console.log('updating served');
            sd.served = served;
        }
        
        if(promised) {
            
            console.log('updating promised');
            sd.promised = promised;
        }
        
        if(invUpdate) {
            console.log(invUpdate);
        }
        
        return sd.save();
    });
}