function(queryParams,db, _) {
    const passcode = queryParams.passcode;
    const serviceDay = queryParams.service_day;
    
    if(!passcode && !serviceDay){
        return {error:'missing required params', passcode};
    }
    
    var query = passcode ? {passcode, active:true} : {_id:serviceDay};
    
    return db.ServiceDay.findOne(query).then(sd=>{
        if(!sd) {
            return {error:'invalid passcode (no active service day found'};
        }
        
        const stations = _.map(sd.stations, 'station._id');
        return db.ServiceStation.find({_id:{$in:stations}}, {org:0}).then(ssList=>{
            const ssMap = _.keyBy(ssList, '_id');
            _.forEach(sd.stations, s=>{
                let id = s.station && s.station._id;
                s.station = ssMap[id];
            });
            return sd;
        })
        
        
    });
}